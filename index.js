'use strict';

const PAGE_ACCESS_TOKEN="EAAEvPCK3Rb8BACkVtE6WX9aZCexd5mDDyb1EKxMRXXZCbqsgly3kQ4VnpP8ZAmQbp7dlRpM6rU28RLl3Tc7N5Hr7lHQKKEMmpsy7mqPZAhwhflxhuYp5j97cUuyqM0ZCtIEJSjK3euUewXPptBzbxhdRogaIjlAUBGTdjaLpCAgZDZD";

// Imports dependencies and set up http server
const
    express = require('express'),
    bodyParser = require('body-parser'),
    app = express().use(bodyParser.json()), // creates express http server
    request = require('request'),
    AWS = require('aws-sdk'),
    axios = require('axios'),
    fs = require('fs'),
    dotenv = require('dotenv').config();

app.use(express.static('public'));

app.use(bodyParser.urlencoded({"extended": false}));
app.use(bodyParser.json());

// Sets server port and logs message on success
app.listen(process.env.PORT || 1337, () => console.log('webhook is listening'));

const pfidList = ["A0Y", "BCG", "A8E", "BQ8"];

// Creates the endpoint for our webhook
app.post('/webhook', (req, res) => {

    let body = req.body;

    // Checks this is an event from a page subscription
    if (body.object === 'page') {

        // Iterates over each entry - there may be multiple if batched
        body.entry.forEach(function(entry) {

            // Gets the body of the webhook event
            let webhook_event = entry.messaging[0];
            console.log(webhook_event);

            let sender_psid = webhook_event.sender.id;
            console.log('Sender PSID: ' + sender_psid);

            // Check if the event is a message or postback and
            // pass the event to the appropriate handler function
            if (webhook_event.message) {
                handleMessage(sender_psid, webhook_event.message);
            } else if (webhook_event.postback) {
                handlePostback(sender_psid, webhook_event.postback);
            }

        });

        // Returns a '200 OK' response to all requests
        res.status(200).send('EVENT_RECEIVED');
    } else {
        // Returns a '404 Not Found' if event is not from a page subscription
        res.sendStatus(404);
    }

});

// Adds support for GET requests to our webhook
app.get('/webhook', (req, res) => {

    // Your verify token. Should be a random string.
    let VERIFY_TOKEN = "prithu"

    // Parse the query params
    let mode = req.query['hub.mode'];
    let token = req.query['hub.verify_token'];
    let challenge = req.query['hub.challenge'];

    // Checks if a token and mode is in the query string of the request
    if (mode && token) {

        // Checks the mode and token sent is correct
        if (mode === 'subscribe' && token === VERIFY_TOKEN) {

            // Responds with the challenge token from the request
            console.log('WEBHOOK_VERIFIED');
            res.status(200).send(challenge);

        } else {
            // Responds with '403 Forbidden' if verify tokens do not match
            res.sendStatus(403);
        }
    }
});

app.get("/", function(req, res) {
    res.send("Hi There! :)");

});

// Handles messages events
 async function handleMessage(sender_psid, received_message) {

     let response;

     try {

         // Checks if the message contains text
         if (received_message.text) {
             // Create the payload for a basic text message, which
             // will be added to the body of our request to the Send API
             let parsed = received_message.text.toLowerCase();

             if (parsed.includes("start")) {
                await handlePostback(sender_psid, {payload: JSON.stringify({type: "start"})});
                return;
             }
             else {
                 response = {
                     "text": "Sorry, I do not understand what that means..."
                 }
             }
         }
         else if (received_message.attachments) {

             callSendAPI(sender_psid, {"text": "Processing your image..."});

             // Get the URL of the message attachment
             let attachment_url = received_message.attachments[0].payload.url;

             // process with vistaprint products
             let newJson = await getGalleryFromUrl(attachment_url);



             callSendAPI(sender_psid, {"text": "Congrats! Here are your results."});

             response = {
                 "attachment": {
                     "type": "template",
                     "payload": {
                         "template_type": "generic",
                         // remove aspect for horizontal: 1.91 : 1
                         // "image_aspect_ratio": "square",
                         "elements": newJson
                     }
                 }
             }

             console.log("Image gallery Response: " + response);
         }

     }
     catch (error){
         response = {"text": "Sorry, I don't understand :("};
         console.log(error);
     }

    // Send the response message
    callSendAPI(sender_psid, response);
}

// Handles messaging_postbacks events
async function handlePostback(sender_psid, received_postback) {
    let response;

    // Get the payload for the postback
    let payload = JSON.parse(received_postback.payload);

    console.log(payload);

    // Set the response based on the postback payload
    if (payload.type === 'start') {
        callSendAPI(sender_psid, { "text": "Let's start making a product together! Select one below..." });
        let newJson = await getDefaultGallery();
        response = {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "generic",
                    // remove aspect for horizontal: 1.91 : 1
                    // "image_aspect_ratio": "square",
                    "elements": newJson
                }
            }
        }
    }
    else if(payload.type === "templateSelection"){
        let pfid = payload.pfid;
        callSendAPI(sender_psid, {"text": "You selected " + productsJson[pfid].name});

        // get templates for that pfid
    }
    else {
        response = { "text": "Oops, try sending another image." }
    }
    // Send the message to acknowledge the postback
    callSendAPI(sender_psid, response);
}

// Sends response messages via the Send API
function callSendAPI(sender_psid, response) {
    // Construct the message body
    let request_body = {
        "recipient": {
            "id": sender_psid
        },
        "message": response
    }

    // Send the HTTP request to the Messenger Platform
    request({
        "uri": "https://graph.facebook.com/v2.6/me/messages",
        "qs": { "access_token": PAGE_ACCESS_TOKEN },
        "method": "POST",
        "json": request_body
    }, (err, res, body) => {
        if (!err) {
            console.log(response);
            console.log('message sent!');
        } else {
            console.error("Unable to send message:" + err);
        }
    });
}


// My code

app.get('/options', (req, res, next) => {
    console.log(req);
    let referer = req.get('Referer');
    if (referer) {
        if (referer.indexOf('www.messenger.com') >= 0) {
            res.setHeader('X-Frame-Options', 'ALLOW-FROM https://www.messenger.com/');
        } else if (referer.indexOf('www.facebook.com') >= 0) {
            res.setHeader('X-Frame-Options', 'ALLOW-FROM https://www.facebook.com/');
        }
    }
    console.log("sending response");
    res.sendFile('public/options.html', {root: __dirname});
});

app.get('/optionspostback', (req, res) => {
    let body = req.query;
    console.log(body);
    let altDocId = body.altDocId;
    let response = {
        "text": body.productText
    };

    res.status(200).send('Please close this window to return to the conversation thread.');
    callSendAPI(body.psid, response);
});

async function getGalleryFromUrl(imageUrl){

    let promises = [];

    for (let i = 0; i < pfidList.length; i++){
        let postData =
            {"LogoUrl": imageUrl,
                "LogoFormat": "png",
                "Pfid": pfidList[i],
                "ShopperKey": "10972878"};

        promises.push(
            new Promise( async (resolve) => {
                let resp = await axios.post('https://www.vistaprint.com/hatchery/rpc/LogoDocument/CreateDocument', postData);
                let altDoc = resp.data.AltDocId;
                // let newUrl = "https://www.vistaprint.com/vp/ns/livepreview.aspx?alt_doc_id=" + altDoc
                let newUrl = "https://utils.hatchery.vistaprint.com/prod/scene/product?type=fbCover&bkg=white&altdocid=" + altDoc;
                await uploadToBucket(altDoc, newUrl);
                let idPair = new Pair(altDoc, pfidList[i]);
                resolve(idPair);
                return idPair;
            }));
    }

    let idList = await Promise.all(promises);
    let jsonElementList = await constructGalleryJson(idList);
    console.log(jsonElementList);
    return jsonElementList;
}

// Amazon S3 Bucket Credentials
const BUCKET_NAME = 'hatchery-chatbot';
const IAM_USER_KEY = 'AKIAI56UE36IJ2N3TPDQ';
const IAM_USER_SECRET = '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';

const BUCKET_PATH_URL = "https://s3.amazonaws.com/hatchery-chatbot/";

let s3bucket = new AWS.S3({
    accessKeyId: IAM_USER_KEY,
    secretAccessKey: IAM_USER_SECRET,
    Bucket: BUCKET_NAME
});

// Separate JSON file with product names and descriptions
let productsJson = JSON.parse(fs.readFileSync("products.json"));


// Utility Pair Class
class Pair {
    constructor(first, second) {
        this.first = first;
        this.second = second;
    }
}

/*
Takes in the url of an image and returns its binary representation.
*/
async function postToAxios(imageUrl){
    let result = await axios.request({
        responseType: 'arraybuffer',
        url: imageUrl,
        method: 'get',
        headers: {
            'ContentType': 'image/png',
        },
    });
    return result.data;
}


/*
Uploads the image url fileData to an Amazon S3 Bucket with name fileName
*/
async function uploadToBucket(fileName, fileData){

    let binaryFile = await postToAxios(fileData);

    var params = {
        Bucket: BUCKET_NAME + "/uploads",
        Key: fileName + ".png",
        Body: binaryFile,
        ContentType: 'image/png',
        ACL: 'public-read'
    };

    await s3bucket.putObject(params, (error, data) => {

        if (error) {
            console.log(error);
        } else {
            console.log(data);
        }
    });
}

/*
Takes in a list of pairs of altDocId to pfid,
Returns a list of json objects that will displayed to the user.
*/
async function constructGalleryJson(listOfPairs) {
    let finalList = [];

    for (let i = 0; i < listOfPairs.length; i++){

        let altDocId = listOfPairs[i].first;
        let pfid = String(listOfPairs[i].second);

        let name = productsJson[pfid].name;
        let description = productsJson[pfid].details;

        let newJson = {
            "title": name,
            "image_url": BUCKET_PATH_URL + "uploads/" + altDocId + ".png",
            "subtitle": description,
            "default_action": {
                "type": "web_url",
                "url": "https://www.vistaprint.com/studio.aspx?alt_doc_id=" + altDocId
            },
            "buttons": [
                {
                    "type": "web_url",
                    "url": "https://www.vistaprint.com/studio.aspx?alt_doc_id=" + altDocId,
                    "title": "Edit in Studio"
                },
                {
                    "type": "web_url",
                    "title": "Edit in Messenger",
                    "url": "https://hatchery-chat-bot.herokuapp.com/options?altDocId=" + altDocId,
                    "webview_height_ratio": "compact",
                    "messenger_extensions": "true"
                },
                {
                    "type": "element_share"
                }
    ]
    }

        finalList.push(newJson);
    }
    return finalList;
}

async function getDefaultGallery() {
    let finalList = [];

    for (let i = 0; i < pfidList.length; i++){
        let pfid = pfidList[i];

        let name = productsJson[pfid].name;
        let description = productsJson[pfid].details;

        let newJson = {
            "title": name,
            "image_url": BUCKET_PATH_URL + "static/" + pfid + ".png",
            "subtitle": description,
            "buttons": [
                {
                    "type": "postback",
                    "payload": JSON.stringify({"type": "templateSelection", "pfid": pfid}),
                    "title": "Select This"
                }
            ]
        }

        finalList.push(newJson);
    }
    return finalList;
}

