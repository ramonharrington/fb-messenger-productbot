var axios = require('axios');

var slack_api = 'https://hooks.slack.com/services/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';

module.exports = {
  getBaseData: function(){
    return {
      channel: '#messenger-phone-cases',
      username: 'messengerbot',
      icon_emoji: ':photobridge:',
    };
  },

  send(data){
    axios.post(slack_api, data);
  },

  sendMsg: function(msg){
    var data = this.getBaseData();
    data.text = msg;

    this.send(data);
  },

  sendDataMsg: function(msg){
    var data = this.getBaseData();
    Object.assign(data, msg);

    this.send(data);
  },
};

