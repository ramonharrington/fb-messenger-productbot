const PROD_PAGE_ACCESS_TOKEN="EAAd5D6LOxyABAAgXKLTidFNJB8C9ZAMIq3AlHdQ54rVGiuAfXhmOQy16yvrcF8pKByXLSO0sNxg2nXv8NHF9Mr46V0Ewm1bTPjEeSQ5ALh4bgsCWHKaix6zxsUU06csrKXe0r3fO9pQjps3GPeRFIKEy7ZAUrjNQQIx31c0gZDZD";

// Separate JSON file with product names and descriptions
var helpers = require('./helpers.js');
var request = require('request');
var slack = require('./slack.js');
let phonesJson = require("../phones.json");

// Sends response messages via the Send API
function callSendAPI(sender_psid, response, PAGE_ACCESS_TOKEN) {

    // Construct the message body
    let request_body = {
        "recipient": {
            "id": sender_psid
        },
        "message": response
    };

    // Send the HTTP request to the Messenger Platform
    request({
        "uri": "https://graph.facebook.com/v2.6/me/messages",
        "qs": {"access_token": PAGE_ACCESS_TOKEN},
        "method": "POST",
        "json": request_body
    }, (err, res, body) => {

        if (!err) {
            console.log('message sent!');
        } else {
            console.error("Unable to send message:" + err);
        }
    });

}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

module.exports = class Facebook {

// Handles messages events
    static async handleMessage(sender_psid, received_message, PAGE_ACCESS_TOKEN, WEBVIEW_URL) {

        let response;
        let slackResponse;

        try {

            // Checks if the message contains text
            if (received_message.text) {
                // Create the payload for a basic text message, which
                // will be added to the body of our request to the Send API
                let parsed = received_message.text.toLowerCase();

                if (parsed.includes("start") || parsed.includes("hi") || parsed.includes("hey") || parsed.includes("hello") || parsed.includes("load")) {
                    if (PAGE_ACCESS_TOKEN === PROD_PAGE_ACCESS_TOKEN) {
                        slackResponse = "User with id " + sender_psid + " started messenging";
                        slack.sendMsg(slackResponse);
                    }
                    response = {
                        "text": "Let's build a phone case! What type of phone do you have?",
                        "quick_replies": [
                            {
                                "content_type": "text",
                                "title": "iPhone",
                                "payload": ""
                            },
                            {
                                "content_type": "text",
                                "title": "Samsung Galaxy",
                                "payload": ""
                            }
                        ]
                    };
                }
                else if(parsed.includes("iphone")){
                    callSendAPI(sender_psid, {"text": "What model iPhone do you have?"}, PAGE_ACCESS_TOKEN);
                    console.log("getting default gallery");
                    let newJson = await helpers.getDefaultGallery(true, sender_psid, WEBVIEW_URL);
                    response = {
                        "attachment": {
                            "type": "template",
                            "payload": {
                                "template_type": "generic",
                                "elements": newJson
                            }
                        }
                    };
                    console.log("default gallery response: ", JSON.stringify(response));
                }
                else if(parsed.includes("samsung")){
                    callSendAPI(sender_psid, {"text": "What model Samsung do you have?"}, PAGE_ACCESS_TOKEN);
                    let newJson = await helpers.getDefaultGallery(false, sender_psid, WEBVIEW_URL);
                    response = {
                        "attachment": {
                            "type": "template",
                            "payload": {
                                "template_type": "generic",
                                "elements": newJson
                            }
                        }
                    };
                }
                else {
                    response = {
                        "text": "Sorry, I do not understand what that means..."
                    }
                }
            } else if (received_message.attachments) {

                response = {"text": "Please note that images uploaded through messenger will lose quality. Upload your image through the webview instead. Please type \"start\""};
            }

        }
        catch (error) {
            response = {"text": "Sorry, I don't understand :("};
            console.log("ERROR: " + error);
        }

        // Send the response message
        callSendAPI(sender_psid, response, PAGE_ACCESS_TOKEN);
    }

// Handles messaging_postbacks events
    static async handlePostback(sender_psid, received_postback, PAGE_ACCESS_TOKEN) {

        let response;

        if (received_postback.payload === 'start' || received_postback === 'start'){
            if (PAGE_ACCESS_TOKEN === PROD_PAGE_ACCESS_TOKEN) {
                let slackResponse = "User with id " + sender_psid + " started messenging";
                slack.sendMsg(slackResponse);
            }
            response = {
                "text": "Let's build a phone case! What type of phone do you have?",
                "quick_replies": [
                    {
                        "content_type": "text",
                        "title": "iPhone",
                        "payload": ""
                    },
                    {
                        "content_type": "text",
                        "title": "Samsung Galaxy",
                        "payload": ""
                    }
                ]
            };
        }
        else {

            let payload = JSON.parse(received_postback.payload);

            let body = payload.body;

            if (payload.type === "photo") {

                callSendAPI(sender_psid, {"text": "Processing your image..."}, PAGE_ACCESS_TOKEN);

                let nextId = await helpers.uploadInitialImageToBucket(payload.body.url);

                await sleep(2000);

                let attachment_url = "https://s3.amazonaws.com/hatchery-chatbot/uploads/" + nextId + ".jpg";

                let cropCoordinates = {
                    "left": body.left, "top": body.top, "cropWidth": body.width,
                    "cropHeight": body.height, "fullHeight": body.origHeight, "fullWidth": body.origWidth
                };


                let newJson = await helpers.getFinalProduct(attachment_url, cropCoordinates, body.pfid);

                callSendAPI(sender_psid, {
                    "attachment": {
                        "type": "template",
                        "payload": {
                            "template_type": "generic",
                            "elements": [newJson]
                        }
                    }
                }, PAGE_ACCESS_TOKEN);

                if (PAGE_ACCESS_TOKEN === PROD_PAGE_ACCESS_TOKEN) {
                    slack.sendMsg("User with id " + sender_psid + " uploaded photo at url: " + "https://s3.amazonaws.com/hatchery-chatbot/uploads/" + nextId + ".jpg");
                    slack.sendMsg("User with id " + sender_psid + " processed photo at url: " + newJson.image_url);
                }

                response = {"text": "Thanks for shopping with Vistaprint. We hope you enjoy your product! Text \"start\" to create another product"};

            }

            else {
                response = {"text": "Oops, I don't understand."};
            }

        }
        callSendAPI(sender_psid, response, PAGE_ACCESS_TOKEN);
    }
};
