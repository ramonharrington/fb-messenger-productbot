let axios = require('axios');
let AWS = require('aws-sdk');

let productsJson = require("../phones.json");

// Amazon S3 Bucket Credentials
const BUCKET_NAME = 'hatchery-chatbot';
const IAM_USER_KEY = 'AKIAJ3SMZNBZEXGX72RA';
const IAM_USER_SECRET = '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';

const BUCKET_PATH_URL = "https://s3.amazonaws.com/hatchery-chatbot/";

let s3bucket = new AWS.S3({
    accessKeyId: IAM_USER_KEY,
    secretAccessKey: IAM_USER_SECRET,
    Bucket: BUCKET_NAME
});

// Helpers needed to generate JSON attachments to send back in facebook
module.exports = class Helpers {

    static async getFinalProduct(imageUrl, cropCoordinates, pfid) {


        let postData = await getLivePreview(imageUrl, pfid, cropCoordinates);

        let newJson = await constructGalleryJson(postData);

        return newJson;
    }

    static async getDefaultGallery(isApple, sender_psid, WEBVIEW_URL) {

        let finalList = [];

        for (var key in productsJson) {

            if ((productsJson[key].brand === "iPhone") === isApple){

                let name = productsJson[key].name;


                let newJson = {
                    "title": name,
                    "image_url": BUCKET_PATH_URL + "public/images/product_shots/" + key + ".png",
                    "buttons": [
                            {
                                "type": "web_url",
                                "title": "Upload photo",
                                "url": WEBVIEW_URL + "f_" + key,
                                "webview_height_ratio": "full",
                                "messenger_extensions": "true"

                            }
                        ]
                };

                finalList.push(newJson);

            }

        }
        return finalList;
    }

    static uploadInitialImageToBucket(imageData) {


        let buf = new Buffer(imageData.replace(/^data:image\/\w+;base64,/, ""), 'base64');

        let imageName = makeid();


        var params = {
            Bucket: BUCKET_NAME + "/uploads",
            Key: imageName + ".jpg",
            Body: buf,
            ContentEncoding: 'base64',
            ContentType: 'image/jpeg',
            ACL: 'public-read'
        };

        return new Promise( resolve => {
            s3bucket.putObject(params, (error, data) => {

                if (error) {
                    console.log(error);
                } else {
                    console.log(data);
                }
            });

            resolve(imageName);
        });

    }

};



/*
Takes in the url of an image and returns its binary representation.
*/
async function postToAxios(imageUrl){


    let result = await axios.request({
        responseType: 'arraybuffer',
        url: imageUrl,
        method: 'get',
        headers: {
            'ContentType': 'image/png',
        },
    });

    return result.data;
}



/*
Uploads the image url fileData to an Amazon S3 Bucket with name fileName
*/
async function uploadToBucket(fileName, fileData){

    let binaryFile = await postToAxios(fileData);

    var params = {
        Bucket: BUCKET_NAME + "/uploads",
        Key: fileName + ".png",
        Body: binaryFile,
        ContentType: 'image/png',
        ACL: 'public-read'
    };

    await s3bucket.putObject(params, (error, data) => {

        if (error) {
            console.log(error);
        } else {
            console.log(data);
        }
    });
}

async function uploadCroppedToBucket(imageData){

    let buf = new Buffer(imageData.replace(/^data:image\/\w+;base64,/, ""), 'base64');

    let imageName = makeid() + "_cropped";


    var params = {
        Bucket: BUCKET_NAME + "/uploads",
        Key: imageName + ".png",
        Body: buf,
        ContentEncoding: 'base64',
        ContentType: 'image/png',
        ACL: 'public-read'
    };

    return new Promise( resolve => {
        s3bucket.putObject(params, (error, data) => {

            if (error) {
                console.log(error);
            } else {
                console.log(data);
            }
        });

        resolve(imageName);
    });

}



/*
Takes in a list of pairs of altDocId to pfid,
Returns a list of json objects that will displayed to the user.
*/
async function constructGalleryJson(postData) {



        let altDocId = postData.AltDocId;
        let editUrl = postData.EditUrl;
        let name = postData.ProductName;

        await uploadToBucket(altDocId, "https://utils.hatchery.vistaprint.com/prod/scene/product?type=fbCover&bkg=white&altdocid=" + altDocId);

        let newJson = {
            "title": name,
            "image_url": BUCKET_PATH_URL + "uploads/" + altDocId + ".png",
            "buttons": [
                {
                    "type": "web_url",
                    "url": "http://www.vistaprint.com/vp/gateway.aspx?S=4336259443&cc=holiday&preurl="+encodeURIComponent("/basket-flow-controller.aspx?alt_doc_id=" + altDocId),
                    "title": "Buy Now"
                },
                {
                    "type": "web_url",
                    "url": editUrl.replace("origin", ""),
                    "title": "Advanced Editing"
                },
                {
                    "type": "element_share"
                }
            ]
        };

        return newJson;
}


async function getLivePreview(imageUrl, pfid, cropCoordinates){


    let firstPost = await axios.request({
        url: "https://uploads.documents.cimpress.io/v1/uploads?url=" + encodeURIComponent(imageUrl) + "&tenant=vbu&process=" + JSON.stringify({"type": "image"}),
        method: 'post'
    });

    let secondPostData = {
        "previewHeight": Math.floor(cropCoordinates.cropHeight),
        "previewWidth": Math.floor(cropCoordinates.cropWidth),
        "designTemplate": "f_" + pfid,
        "TextFieldPurposeNameToValue": {}
    };

    let secondPost = await axios.post("http://originwww.vistaprint.com/hatchery/rpc/assettransferapi/GenerateDesignPreview", secondPostData);

    let thirdPostData;

    let cropPercentages = getCropPercentages(cropCoordinates);

    thirdPostData = {
        "previewHeight": Math.floor(cropCoordinates.cropHeight),
        "previewWidth": Math.floor(cropCoordinates.cropWidth),
        "designTemplate": secondPost.data.AltDocId,
        "TextFieldPurposeNameToValue": {},
        "images": [{"id": firstPost.data[0].vp_uploadedId, "imageType": "picture"}],
        "itemMetaData": {"vpls_img_1" : {"imageUsage": "picture"}, "vpls_div_0" : {"imageUsage": "picture"}},
        "saveDocument": true,
        "cropMode": "crop",
        "cropTop": cropPercentages.top,
        "cropLeft": cropPercentages.left,
        "cropRight": cropPercentages.right,
        "cropBottom": cropPercentages.bottom
    }


    let thirdPost = await axios.post("http://originwww.vistaprint.com/hatchery/rpc/assettransferapi/AssetTransfer", thirdPostData);

    thirdPost.data.pfid = pfid;

    return thirdPost.data;

}

function getCropPercentages(cropCoordinates){
    let left = cropCoordinates.left;
    let top = cropCoordinates.top;
    let cropHeight = cropCoordinates.cropHeight;
    let cropWidth = cropCoordinates.cropWidth;
    let fullHeight = cropCoordinates.fullHeight;
    let fullWidth = cropCoordinates.fullWidth;

    let leftPercentage = (left / fullWidth);
    let topPercentage = (top / fullHeight);
    let bottomPercentage = (fullHeight - (top + cropHeight)) / fullHeight;
    let rightPercentage = (fullWidth - (left + cropWidth)) / fullWidth;

    return {"top": topPercentage, "left": leftPercentage, "right": rightPercentage, "bottom": bottomPercentage};

}


function makeid() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < 10; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}
