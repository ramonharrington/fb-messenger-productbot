'use strict';

var facebook = require('../lib/facebook.js');

// Creates the endpoint for our webhook
module.exports.webhookPost = (event, context, callback) => {

    let body = JSON.parse(event.body);

    let PAGE_ACCESS_TOKEN = process.env.PAGE_ACCESS_TOKEN;
    let WEBVIEW_URL = process.env.WEBVIEW_URL;

    let response = {
        statusCode: 200,
    };

    console.log("NEW FB EVENT: " + event.body);

    // Checks this is an event from a page subscription
    if (body.object === 'page') {

        // Iterates over each entry - there may be multiple if batched
        body.entry.forEach(function(entry) {

            // Gets the body of the webhook event
            let webhook_event = entry.messaging[0];
            console.log(webhook_event);

            let sender_psid = webhook_event.sender.id;
            console.log('Sender PSID: ' + sender_psid);

            // Check if the event is a message or postback and
            // pass the event to the appropriate handler function

            if (webhook_event.message) {
                facebook.handleMessage(sender_psid, webhook_event.message, PAGE_ACCESS_TOKEN, WEBVIEW_URL);
            } else if (webhook_event.postback) {
                facebook.handlePostback(sender_psid, webhook_event.postback, PAGE_ACCESS_TOKEN);
            }

        });

        // Returns a '200 OK' response to all requests

        callback(null, response);
        return;
    } else {
        // Returns a '404 Not Found' if event is not from a page subscription
        callback(`Received not a page subscription ${body.object}` , {
            statusCode: 404,
        })
    }

};

module.exports.webhookGet = (event, context, callback) => {

    // Your verify token. Should be a random string.
    let VERIFY_TOKEN = process.env.VERIFY_TOKEN;

    // Parse the query params
    let mode = event.queryStringParameters['hub.mode'];
    let token = event.queryStringParameters['hub.verify_token'];
    let challenge = event.queryStringParameters['hub.challenge'];

    // Checks if a token and mode is in the query string of the request
    if (mode && token) {

        // Checks the mode and token sent is correct
        if (mode === 'subscribe' && token === VERIFY_TOKEN) {

            // Responds with the challenge token from the request
            console.log('WEBHOOK_VERIFIED');
            callback(null, {
                statusCode: 200,
                body: challenge
            })
            return;

        } else {
            // Responds with '403 Forbidden' if verify tokens do not match
            callback(`403 Forbidden`, {
                statusCode: 403,
            })
            return;
        }
    }

    callback(null, {
        statusCode: 404,
    })

};

// endpoint to post to for s3
module.exports.photo = async (event, context, callback) => {

    let body = JSON.parse(event.body);

    let PAGE_ACCESS_TOKEN = process.env.PAGE_ACCESS_TOKEN;

    let payload = {};
    payload.type = "photo";
    payload.body = body;

    facebook.handlePostback(body.psid, {"payload": JSON.stringify(payload)}, PAGE_ACCESS_TOKEN);

    const response = {
        statusCode: 200,
        headers: {
            "Access-Control-Allow-Origin": "*"
        },
        body: JSON.stringify({"message": "Photo"})
    };

    callback(null, response);

};



