"use strict";

const POST_URL = "https://fbm.hatchery.vistaprint.com/dev/phoneCases/photo";

let imageSrc;

(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.com/en_US/messenger.Extensions.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'Messenger'));

function isNotMobileDevice(){
    return !((typeof window.orientation !== "undefined") || (navigator.userAgent.indexOf('IEMobile') !== -1));
}


function getParameterByName(name) {
    let url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function submitClick(){


    let leftPixel = $('#mask').offset().left - $('#resize-image').offset().left;
    let topPixel = $('#mask').offset().top - $('#resize-image').offset().top;
    let finalWidth = $('#mask').width();
    let finalHeight = $('#mask').height();
    let initHeight = $('#resize-image').height();
    let initWidth = $('#resize-image').width();
    let sent = getParameterByName("sender");
    let pfid = getParameterByName('pfid');


    let body = {
        url: imageSrc,
        left: leftPixel,
        top: topPixel,
        width: finalWidth,
        height: finalHeight,
        origHeight: initHeight,
        origWidth: initWidth,
        psid: sent,
        pfid: pfid
    };

    console.log(imageSrc);
    console.log(body);

    $.ajax({
        url: POST_URL,
        type: "POST",
        crossDomain: true,
        data: JSON.stringify(body),
        processData: false,
        contentType: "application/json",
        complete: function (data, status) {
            MessengerExtensions.requestCloseBrowser(function success() {
                console.log("webview closing");
            }, function error(err) {
                console.log(err);
            });
        }
    });

    document.getElementById("second-scene").style.display = "none";
    document.getElementById("loader").style.display = "flex";

}

function getOrientation(file, callback) {
    var reader = new FileReader();
    reader.onload = function(e) {

        var view = new DataView(e.target.result);
        if (view.getUint16(0, false) != 0xFFD8)
        {
            return callback(-2);
        }
        var length = view.byteLength, offset = 2;
        while (offset < length)
        {
            if (view.getUint16(offset+2, false) <= 8) return callback(-1);
            var marker = view.getUint16(offset, false);
            offset += 2;
            if (marker == 0xFFE1)
            {
                if (view.getUint32(offset += 2, false) != 0x45786966)
                {
                    return callback(-1);
                }

                var little = view.getUint16(offset += 6, false) == 0x4949;
                offset += view.getUint32(offset + 4, little);
                var tags = view.getUint16(offset, little);
                offset += 2;
                for (var i = 0; i < tags; i++)
                {
                    if (view.getUint16(offset + (i * 12), little) == 0x0112)
                    {
                        return callback(view.getUint16(offset + (i * 12) + 8, little));
                    }
                }
            }
            else if ((marker & 0xFF00) != 0xFF00)
            {
                break;
            }
            else
            {
                offset += view.getUint16(offset, false);
            }
        }
        return callback(-1);
    };
    reader.readAsArrayBuffer(file);
}


var resizeableImage = function (image_target) {

    // Some variable and settings
    var $container,
        orig_src = new Image(),
        image_target = $(image_target).get(0),
        event_state = {},
        constrain = false,
        min_width = 100, // Change as required
        min_height = 150,
        max_width = 1800, // Change as required
        max_height = 1900,
        init_height = 400,
        resize_canvas = document.createElement('canvas'),
        imageData = null;

    function init() {

        //load a file with html5 file api
        $('#photoInput').change(function (evt) {
            document.getElementById("first-scene").style.display = "none";
            document.getElementById("second-scene").style.display = "flex";
            var files = evt.target.files; // FileList object
            var fileReader = new FileReader();

            fileReader.onload = function (e) {
                imageData = fileReader.result;
                loadData(files[0]);
            }
            fileReader.readAsDataURL(files[0]);
        });

        $('#rotate').click(function(){
            rotate();
        });


        var mask = document.getElementById("mask");
        mask.src = "https://s3.amazonaws.com/hatchery-chatbot/masks/" + "SKU559004" + "_mask.png";
        mask.style.left = 0;
        mask.style.top = 0;

        // When resizing, we will always use this copy of the original as the base
        orig_src.src = image_target.src;

        // Wrap the image with the container and add resize handles

        $(image_target).height(init_height)
            .wrap('<div class="resize-container"></div>')
            .before('<span class="resize-handle resize-handle-nw"></span>')
            .before('<span class="resize-handle resize-handle-ne"></span>')
            .after('<span class="resize-handle resize-handle-se"></span>')
            .after('<span class="resize-handle resize-handle-sw"></span>');




        // Assign the container to a variable
        $container = $('.resize-container');

        $container.prepend('<div class="resize-container-ontop"></div>');

        // Add events
        $container.on('mousedown touchstart', '.resize-handle', startResize);
        $container.on('mousedown touchstart', '.resize-container-ontop', startMoving);
    };

    function loadData(file) {

        imageSrc = imageData;
        orig_src.src = imageData;

        let img = document.getElementById('resize-image');

        console.log(isNotMobileDevice());
        if (isNotMobileDevice()) {

            getOrientation(file, function (orientation_id) {

                console.log(orientation_id);
                switch (orientation_id) {
                    case 3:
                        img.style.transform = "rotate(180deg)";
                        break;
                    case 6:
                        let rotate_canvas = document.createElement('canvas');

                        let ctx = rotate_canvas.getContext('2d');

                        let height = $(image_target).height();
                        let width = $(image_target).width();
                        rotate_canvas.width = height;
                        rotate_canvas.height = width;
                        ctx.transform(0, 1, -1, 0, height , 0);
                        ctx.drawImage(image_target, 0, 0, width, height);
                        let newUrl = rotate_canvas.toDataURL("image/png");
                        $(image_target).attr('src', newUrl);
                        orig_src.src = newUrl;

                        break;
                    case 8:
                        img.style.transform = "rotate(-90deg)";
                        break;
                    default:
                        break;
                }
            });
        }
        img.src = imageData;
    };

    function rotate(){

        let rotate_canvas = document.createElement('canvas');

        let ctx = rotate_canvas.getContext('2d');

        let height = $(image_target).height();
        let width = $(image_target).width();
        rotate_canvas.width = height;
        rotate_canvas.height = width;
        ctx.transform(0, 1, -1, 0, height , 0);
        ctx.drawImage(image_target, 0, 0, width, height);
        let newUrl = rotate_canvas.toDataURL("image/png", 1.0);
        $(image_target).attr('src', newUrl);
        orig_src.src = newUrl;
        imageSrc = newUrl;
        // $(image_target).height($container.height());
        // $(image_target).width($container.width());
        // $(image_target).offset({ 'left': $container.offset().left, 'top': $container.offset().top });
    }

    function startResize(e) {

        e.preventDefault();
        e.stopPropagation();
        saveEventState(e);
        $(document).on('mousemove touchmove', resizing);
        $(document).on('mouseup touchend', endResize);
    };

    function endResize (e) {

        e.preventDefault();
        $(document).off('mouseup touchend', endResize);
        $(document).off('mousemove touchmove', resizing);
    };

    function saveEventState (e) {
        // Save the initial event details and container state
        event_state.container_width = $container.width();
        event_state.container_height = $container.height();
        event_state.container_left = $container.offset().left;
        event_state.container_top = $container.offset().top;
        event_state.mouse_x = (e.clientX || e.pageX || e.originalEvent.touches[0].clientX) + $(window).scrollLeft();
        event_state.mouse_y = (e.clientY || e.pageY || e.originalEvent.touches[0].clientY) + $(window).scrollTop();

        // This is a fix for mobile safari
        // For some reason it does not allow a direct copy of the touches property
        if (typeof e.originalEvent.touches !== 'undefined') {
            event_state.touches = [];
            $.each(e.originalEvent.touches, function (i, ob) {
                event_state.touches[i] = {};
                event_state.touches[i].clientX = 0 + ob.clientX;
                event_state.touches[i].clientY = 0 + ob.clientY;
            });
        }
        event_state.evnt = e;
    };

    function resizing (e) {

        var mouse = {}, width, height, left, top, offset = $container.offset();
        mouse.x = (e.clientX || e.pageX || e.originalEvent.touches[0].clientX) + $(window).scrollLeft();
        mouse.y = (e.clientY || e.pageY || e.originalEvent.touches[0].clientY) + $(window).scrollTop();

        // Position image differently depending on the corner dragged and constraints
        if ($(event_state.evnt.target).hasClass('resize-handle-se')) {
            width = mouse.x - event_state.container_left;
            height = mouse.y - event_state.container_top;
            left = event_state.container_left;
            top = event_state.container_top;
        } else if ($(event_state.evnt.target).hasClass('resize-handle-sw')) {
            width = event_state.container_width - (mouse.x - event_state.container_left);
            height = mouse.y - event_state.container_top;
            left = mouse.x;
            top = event_state.container_top;
        } else if ($(event_state.evnt.target).hasClass('resize-handle-nw')) {
            width = event_state.container_width - (mouse.x - event_state.container_left);
            height = event_state.container_height - (mouse.y - event_state.container_top);
            left = mouse.x;
            top = mouse.y;
            if (constrain || e.shiftKey) {
                top = mouse.y - ((width / orig_src.width * orig_src.height) - height);
            }
        } else if ($(event_state.evnt.target).hasClass('resize-handle-ne')) {
            width = mouse.x - event_state.container_left;
            height = event_state.container_height - (mouse.y - event_state.container_top);
            left = event_state.container_left;
            top = mouse.y;
            if (constrain || e.shiftKey) {
                top = mouse.y - ((width / orig_src.width * orig_src.height) - height);
            }
        }

        // Optionally maintain aspect ratio
        if (constrain || e.shiftKey) {
            height = width / orig_src.width * orig_src.height;
        }

        if (width > min_width && height > min_height && width < max_width && height < max_height) {
            // To improve performance you might limit how often resizeImage() is called
            resizeImage(width, height);
            // Without this Firefox will not re-calculate the the image dimensions until drag end
            $container.offset({ 'left': left, 'top': top });
        }
    }

    function resizeImage(width, height) {

        $(image_target).width(width).height(height);
    };


    function startMoving (e) {
        e.preventDefault();
        e.stopPropagation();
        saveEventState(e);
        $(document).on('mousemove touchmove', moving);
        $(document).on('mouseup touchend', endMoving);
    };

    function endMoving (e) {
        e.preventDefault();
        $(document).off('mouseup touchend', endMoving);
        $(document).off('mousemove touchmove', moving);
    };

    function moving (e) {
        var mouse = {}, touches;
        e.preventDefault();
        e.stopPropagation();

        touches = e.originalEvent.touches;

        mouse.x = (e.clientX || e.pageX || touches[0].clientX) + $(window).scrollLeft();
        mouse.y = (e.clientY || e.pageY || touches[0].clientY) + $(window).scrollTop();

        // constraint for movement
        $container.offset({
            'left': mouse.x - (event_state.mouse_x - event_state.container_left),
            'top': mouse.y - (event_state.mouse_y - event_state.container_top)
        });
        // Watch for pinch zoom gesture while moving
        if (event_state.touches && event_state.touches.length > 1 && touches.length > 1) {
            var width = event_state.container_width, height = event_state.container_height;
            var a = event_state.touches[0].clientX - event_state.touches[1].clientX;
            a = a * a;
            var b = event_state.touches[0].clientY - event_state.touches[1].clientY;
            b = b * b;
            var dist1 = Math.sqrt(a + b);

            a = e.originalEvent.touches[0].clientX - touches[1].clientX;
            a = a * a;
            b = e.originalEvent.touches[0].clientY - touches[1].clientY;
            b = b * b;
            var dist2 = Math.sqrt(a + b);

            var ratio = dist2 / dist1;

            width = width * ratio;
            height = height * ratio;
            // To improve performance you might limit how often resizeImage() is called
            if (width > min_width && height > min_height) {
                resizeImage(width, height);
            }
        }
    };

    init();
};

// Kick everything off with the target image
resizeableImage($('.resize-image'));